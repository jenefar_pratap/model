package com.rigor.one.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jenefar
 */
@Document(collection = "candidates")
public class Candidate {

    private String _id;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private String dateOfBirth;
    private String nationality;
    private boolean workEligibility;
    private String address;
    private String country;
    private String state;
    private String city;
    private String currency;
    private String currentCTC;
    private String expectedCTC;
    private String maritalStatus;
    private Integer rating;
    private String resumePath;
    private List<String> skills;
    private String photoUrl;
    private String jobId;
    private String gender;

    private String currentCompany;
    private String currentDesignation;
    private String highestDegree;
    private String specialization;
    private String currentLocation;
    private String noticePeriod;
    private String totalExperience;
    private String relevantExperience;

    private Referrer referrer;

    private Map<String,String> socialProfiles=new HashMap<>();

    private Date uploadedDate=new Date();

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public boolean isWorkEligibility() {
        return workEligibility;
    }

    public void setWorkEligibility(boolean workEligibility) {
        this.workEligibility = workEligibility;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getExpectedCTC() {
        return expectedCTC;
    }

    public void setExpectedCTC(String expectedCTC) {
        this.expectedCTC = expectedCTC;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getResumePath() {
        return resumePath;
    }

    public void setResumePath(String resumePath) {
        this.resumePath = resumePath;
    }

    public Date getUploadedDate() {
        return uploadedDate;
    }

    public void setUploadedDate(Date uploadedDate) {
        this.uploadedDate = uploadedDate;
    }

    public List<String> getSkills() {
        return skills;
    }

    public void setSkills(List<String> skills) {
        this.skills = skills;
    }

    public String getCurrentCompany() {
        return currentCompany;
    }

    public void setCurrentCompany(String currentCompany) {
        this.currentCompany = currentCompany;
    }

    public String getCurrentDesignation() {
        return currentDesignation;
    }

    public void setCurrentDesignation(String currentDesignation) {
        this.currentDesignation = currentDesignation;
    }

    public String getHighestDegree() {
        return highestDegree;
    }

    public void setHighestDegree(String highestDegree) {
        this.highestDegree = highestDegree;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }

    public String getNoticePeriod() {
        return noticePeriod;
    }

    public void setNoticePeriod(String noticePeriod) {
        this.noticePeriod = noticePeriod;
    }

    public String getTotalExperience() {
        return totalExperience;
    }

    public void setTotalExperience(String totalExperience) {
        this.totalExperience = totalExperience;
    }

    public String getRelevantExperience() {
        return relevantExperience;
    }

    public void setRelevantExperience(String relevantExperience) {
        this.relevantExperience = relevantExperience;
    }

    public String getCurrentCTC() {
        return currentCTC;
    }

    public void setCurrentCTC(String currentCTC) {
        this.currentCTC = currentCTC;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Map<String, String> getSocialProfiles() {
        return socialProfiles;
    }

    public void setSocialProfiles(Map<String, String> socialProfiles) {
        this.socialProfiles = socialProfiles;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Referrer getReferrer() {
        return referrer;
    }

    public void setReferrer(Referrer referrer) {
        this.referrer = referrer;
    }

    /**
     * {
     firstName            : { type: String, required: true },
     lastName             : { type: String, required: true },
     email                : { type: String, required: true },
     phoneNumber          : { type: String, required: true },
     dateOfBirth          : { type: String, required: true },
     nationality          : { type: String, required: true },
     workEligibility      : { type: Boolean },
     address              : { type: String, required: true },
     country              : { type: String, required: true },
     state                : { type: String, required: true },
     city                 : { type: String, required: true },
     currency             : { type: String },
     expectedCTC          : { type: String },
     maritalStatus        : { type: String, required: true },
     rating               : { type: Number },
     referral             : {
     referrer : { type: String, required: true },
     info     : { type: String }
     },
     language             : [{
     name : { type: String },
     proficiency: { type: String}
     }],
     experience           : [{
     designation: { type: String },
     company    : { type: String },
     startDate  : { type: Date   },
     endDate    : { type: Date   },
     location   : { type: String },
     description: { type: String }
     }],

     education            : [{
     institute  : { type: String },
     degree     : { type: String },
     major      : { type: String },
     startDate  : { type: Date },
     endDate    : { type: Date },
     cgpa       : { type: String },
     description: { type: String }
     }],

     projects              : [{
     name       : { type: String },
     startDate  : { type: Date },
     endDate    : { type: Date },
     description: { type: String }
     }],

     certifications         : [{
     name                  : { type: String },
     certificationNumber   : { type: String },
     certificationAuthority: { type: String },
     certificationDate     : { type: Date },
     description           : { type: String }
     }],

     resumePath              : { type: String, required: true },
     uploadedDate            : { type: Date, default: Date.now }

     }

     */
}
