package com.rigor.one.domain;

/**
 * @author jenefar
 */
public class NotificationParams {

    private String jobId;
    private String designation;
    private String consultantResumeUploadUrl;
    private String duplicateProfileEmail;

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getConsultantResumeUploadUrl() {
        return consultantResumeUploadUrl;
    }

    public void setConsultantResumeUploadUrl(String consultantResumeUploadUrl) {
        this.consultantResumeUploadUrl = consultantResumeUploadUrl;
    }

    public String getDuplicateProfileEmail() {
        return duplicateProfileEmail;
    }

    public void setDuplicateProfileEmail(String duplicateProfileEmail) {
        this.duplicateProfileEmail = duplicateProfileEmail;
    }
}
