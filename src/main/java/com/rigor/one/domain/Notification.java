package com.rigor.one.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * @author jenefar
 */
@Document(collection = "notification")
public class Notification {

    @Id
    @JsonProperty
    private String _id;
    @JsonProperty
    private String userId;
    private String userEmail;
    private String sender;
    private String recipient;
    private String subject;
    private String message;
    private String templateType;
    private Integer type;
    private Integer templateCode;
    private NotificationParams params;
    private Date createdAt=new Date();


    public enum Type{
        EMAIL(1),WEBAPP(2),SMS(3);
        Integer type;

        Type(Integer type) {
            this.type = type;
        }

        public Integer getNotificationType() {
            return type;
        }
    }

    public enum Template {
        COMPANY_ADMIN_VERIFICATION(1),
        ACCOUNT_ACTIVATION(2),
        SUBUSER_SIGNUP_REQUEST(3),
        JOB_POST(4),
        DUPLICATE_PROFILE(5);

        Integer templateCode;

        Template(Integer templateCode) {
            this.templateCode = templateCode;
        }

        public Integer getTemplateCode() {
            return templateCode;
        }
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTemplateType() {
        return templateType;
    }

    public void setTemplateType(String templateType) {
        this.templateType = templateType;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public NotificationParams getParams() {
        return params;
    }

    public void setParams(NotificationParams params) {
        this.params = params;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getTemplateCode() {
        return templateCode;
    }

    public void setTemplateCode(Integer templateCode) {
        this.templateCode = templateCode;
    }
}

